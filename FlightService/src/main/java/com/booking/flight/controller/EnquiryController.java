package com.booking.flight.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.booking.flght.service.FlightDetailsServiceImpl;
import com.booking.flight.entity.FlightDetails;

@RestController
public class EnquiryController {
	
	@Autowired
	FlightDetailsServiceImpl flightDetailsServiceImpl;

	@GetMapping("/flight/enquiry/{source}/{destination}/{date}")
	public List<FlightDetails> enquiry(@PathVariable(value = "source") String source, @PathVariable(value = "destination") String destination,
				@PathVariable(value = "date") String date) {
	    List<FlightDetails> flights = flightDetailsServiceImpl.getFlightDetails(source, destination, date);
	     
	    return flights;
	}

}
