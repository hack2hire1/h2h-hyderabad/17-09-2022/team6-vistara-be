package com.booking.flight.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.booking.flight.entity.FlightDetails;

@Repository
public interface FlightDetailsRepo extends JpaRepository<FlightDetails, Long>{
	
	
	public List<FlightDetails> findBySourceAndDesinationAndDae(String source, String destination,  String date);
	
}
