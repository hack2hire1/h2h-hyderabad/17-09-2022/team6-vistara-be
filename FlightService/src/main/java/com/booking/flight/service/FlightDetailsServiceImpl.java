package com.booking.flight.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.booking.flight.entity.FlightDetails;
import com.booking.flight.repo.FlightDetailsRepo;

@Service
public class FlightDetailsServiceImpl  implements FlightDetailsService{
	
	@Autowired
	FlightDetailsRepo repo;
	
	public List<FlightDetails> getFlightDetails(String source,String destination,String date){
		
		return repo.findBySourceAndDesinationAndDae(source, destination, date);
		
	}

}
