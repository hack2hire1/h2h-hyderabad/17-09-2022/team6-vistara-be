package com.booking.flight.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class FlightDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
private int flightId;
	
	private String filghtName;
	
	private Double NoOfSeats;
	
	private  String source;
	
	private String destination;

	private String startTime;
	
	private String endTime;
	
	private String date;
	

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public String getFilghtName() {
		return filghtName;
	}

	public void setFilghtName(String filghtName) {
		this.filghtName = filghtName;
	}

	public Double getNoOfSeats() {
		return NoOfSeats;
	}

	public void setNoOfSeats(Double noOfSeats) {
		NoOfSeats = noOfSeats;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return "FlightDetails [flightId=" + flightId + ", filghtName=" + filghtName + ", NoOfSeats=" + NoOfSeats
				+ ", source=" + source + ", destination=" + destination + ", startTime=" + startTime + ", endTime="
				+ endTime + "]";
	}
	
	
	

}
