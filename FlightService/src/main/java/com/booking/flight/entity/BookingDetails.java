package com.booking.flight.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class BookingDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int fligtId;
	private String fligt_name;
	private String bookingStatus;
	private String fligtStatus;
	private String flightSource;
	private String flightDestination;

	public int getFligtId() {
		return fligtId;
	}

	public void setFligtId(int fligtId) {
		this.fligtId = fligtId;
	}

	public String getFligt_name() {
		return fligt_name;
	}

	public void setFligt_name(String fligt_name) {
		this.fligt_name = fligt_name;
	}

	public String getBookingStatus() {
		return bookingStatus;
	}

	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	public String getFligtStatus() {
		return fligtStatus;
	}

	public void setFligtStatus(String fligtStatus) {
		this.fligtStatus = fligtStatus;
	}

	public String getFlightSource() {
		return flightSource;
	}

	public void setFlightSource(String flightSource) {
		this.flightSource = flightSource;
	}

	public String getFlightDestination() {
		return flightDestination;
	}

	public void setFlightDestination(String flightDestination) {
		this.flightDestination = flightDestination;
	}

}
